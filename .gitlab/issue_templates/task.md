### Task to solve

<!-- What task should you solve? Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)." -->

### Goals
<!--
SMART:

- S: Specific - define the goal as specifically as possible
- M: Measurable - how can we assess success?
- A: Attractive - must be an improvement and fun
- R: Realistic - must be doable
- T: Timable - set a Due Date
-->

### Proposal

<!-- How are we going to solve the task? Try to include the user journey! https://about.gitlab.com/handbook/journeys/#user-journey -->

